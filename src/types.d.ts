import { Props, Flags, Args } from './constants';

export interface PropsAndArgs {
  props: Record<string, string>;
  flags: Flags[];
  args: Args[];
}

export interface Versions {
  instanceVersion: string,
  homeVersion: string,
  otherVersions: string[]
}

export interface ErrorType {
  id: string,
  status: number,
  type: string,
  title: string,
  detail: string,
  content: string,
  contentJava: string,
  responseAdd: string,
  publishVersion: string,
  service: string,
  notes: string
}
