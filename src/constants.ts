'use strict';

export enum Args {
  NONE = 100,
  CONFIG,
  VERSION,
  INDEX,
  ERROR_TYPES
};

export enum Props {
  NONE = 200,
  OUT,
  IN
};

export enum Flags {
  NONE = 300,
  DEBUG
};

export const semVerRegEx = /^v(\d+\.){2}\d+(-[a-z]+(\.\d+)?)?$/;

export const buildVerRegEx = /^main-[a-f0-9]{8}$/;
