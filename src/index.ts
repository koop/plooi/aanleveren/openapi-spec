import { readFile, writeFile, stat, mkdir, readdir } from 'fs/promises';
import { fileURLToPath } from 'url';
import path, { dirname } from 'path';
import yaml from 'js-yaml';
import handlebars from 'handlebars';
import { Props, Flags, Args, semVerRegEx, buildVerRegEx } from './constants';
import type { ErrorType, PropsAndArgs, Versions } from './types';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

handlebars.registerHelper('isTag', value => semVerRegEx.test(value));

const enumLookup = <E>(enumInstance : E, value : string, defaultValue : E[keyof E]) : E[keyof E] =>
  enumInstance[<keyof typeof enumInstance>value.replace(/(^[^A-Z]+)([A-Z])/, '$1_$2').toUpperCase()] || defaultValue;

// Extract properties and arguments from command line
const getPropsAndArgs = () : PropsAndArgs => {
  const input = process.argv
    .map(arg => arg.split('='))
    .flat();
  const propsIndices = [] as Array<number>;
  return {
    props: input.reduce((accum, value, index) => {
      const prop = value.startsWith('-')
        ? enumLookup(Props, value.replace(/^-+/, ''), Props.NONE)
        : Props.NONE;
      if (prop !== Props.NONE) {
        propsIndices.push(index);
        accum = Object.assign({}, accum, { [prop]: input[index + 1] });
      }
      return accum;
    }, {} as Record<string, string>),
    flags: input.filter((item, index) =>
        item.startsWith('-') &&
        propsIndices.findIndex(indexItem => indexItem === index || indexItem === index - 1) === -1)
      .map(item => enumLookup(Flags, item.replace(/^-+/, ''), Flags.NONE))
      .filter(flag => flag !== Flags.NONE),
    args: input.filter((item, index) =>
        !item.startsWith('-') &&
        propsIndices.findIndex(indexItem => indexItem === index - 1) === -1)
      .map(item => enumLookup(Args, item.replace(/^-+/, ''), Args.NONE))
      .filter(arg => arg !== Args.NONE)
  };
};

const getVersions = () : Versions => {
  const isDownstreamVersion = process.env.VERSION_TAG === `v${process.env.npm_package_version}`;
  const isCommitVersion = process.env.CI_COMMIT_TAG === `v${process.env.npm_package_version}`;
  const hasHomeVersion = typeof process.env.HOME_TAG === 'string' &&
      (semVerRegEx.test(process.env.HOME_TAG) || buildVerRegEx.test(process.env.HOME_TAG));
  if (!hasHomeVersion) {
    console.error('Missing home version');
    process.exitCode = 1;
  }
  const otherVersions = typeof process.env.PUBLISH_TAGS === 'string'
    ? process.env.PUBLISH_TAGS.split(/\s*,\s*/).filter(tag => semVerRegEx.test(tag) || buildVerRegEx.test(tag))
    : [] as string[];
  const uniqueOtherVersions = [ ...new Set(otherVersions) ];
  return {
    instanceVersion: isDownstreamVersion || isCommitVersion
      ? `v${process.env.npm_package_version}`
      : `${process.env.CI_COMMIT_BRANCH || 'main'}-${process.env.CI_COMMIT_SHORT_SHA || '00000000'} (v${process.env.npm_package_version})`,
    homeVersion: hasHomeVersion
      ? `${process.env.HOME_TAG}`
      : '',
    otherVersions: uniqueOtherVersions
  };
};

const loadFile = async (fromUrl : URL) => {
  const localPath = path.resolve(__dirname, '..', `${fromUrl.hostname}${fromUrl.pathname}`);
  const fileText = await readFile(localPath, { encoding: 'utf8' as BufferEncoding })
    .catch((reason) => {
      console.error(`File ${fromUrl.href} couldn't be read: ${reason}`);
      return null;
    });
  return {
    ok: typeof fileText === 'string',
    text: () => fileText || ''
  };
};

const storeFile = async (outPath : string, toUrl : URL, contents : string) => {
  const localPath = path.resolve(__dirname, '..', outPath || 'dist/versies/unknown', `${toUrl.hostname}${toUrl.pathname}`);

  // ensure directory exists
  let dirExists = false;
  const dirPath = path.dirname(localPath);
  const dirStat = await stat(dirPath).catch((reason) => {
    console.info(`Directory ${dirPath} doesn't exist yet. It will be created.`);
    return null;
  });
  if (dirStat && dirStat.isDirectory()) {
    dirExists = true;
  }
  if (dirExists !== true) {
    await mkdir(dirPath, { recursive: true });
  }

  // write file
  const result = await writeFile(localPath, contents)
    .catch((reason) => {
      console.error(`File ${toUrl.href} couldn't be written: ${reason}`);
      return null;
    });
  return {
    ok: typeof result === 'undefined'
  }
};

const fillTemplates = async (propsAndArgs : PropsAndArgs, versions : Versions) => {
  const pageFromUrl = new URL('file://statisch/index.html');
  const pageToUrl = new URL('file://index.html');
  const pageTemplateLoader = await loadFile(pageFromUrl);
  if (pageTemplateLoader.ok === true) {
    const pageTemplate = handlebars.compile(pageTemplateLoader.text());
    await storeFile(propsAndArgs.props[Props.OUT], pageToUrl, pageTemplate(versions));
  }

  const specVersionUrl = new URL('file://fragmenten/version.yaml');
  const specVersionTemplateLoader = await loadFile(specVersionUrl);
  if (specVersionTemplateLoader.ok === true) {
    const specVersionTemplate = handlebars.compile(specVersionTemplateLoader.text());
    await storeFile('.', specVersionUrl, specVersionTemplate(versions));
  }
};

const generateConfig = async (versions : Versions) => {
  const configUrl = new URL('file://.tag-template.gitlab-ci.yml');
  const configTemplateLoader = await loadFile(configUrl);
  if (configTemplateLoader.ok === true) {
    const configTemplate = handlebars.compile(configTemplateLoader.text());
    await storeFile('.', new URL('file://.tag.gitlab-ci.yml'), configTemplate(versions));
  }
};

const populateIndex = async (propsAndArgs : PropsAndArgs) => {
  const dirPath = path.resolve(__dirname, '..', propsAndArgs.props[Props.IN]);

  // ensure directory exists
  let dirExists = false;
  const dirStat = await stat(dirPath).catch(reason => {
    console.debug(`Directory ${dirPath} doesn't exist.`, reason);
    return null;
  });
  if (dirStat && dirStat.isDirectory()) {
    dirExists = true;
  }
  if (!dirExists) {
    return Promise.reject('Directory to index does not exist');
  }
  const dirContents = await readdir(dirPath).catch(reason => {
    console.debug(`Directory ${dirPath} could not be read.`, reason);
    return null;
  });
  if (!dirContents || !Array.isArray(dirContents)) {
    return Promise.reject('Nothing found to index');
  }

  const versions : Versions = {
    instanceVersion: '',
    homeVersion: '../',
    otherVersions: dirContents.filter(item => semVerRegEx.test(item) || buildVerRegEx.test(item))
      .map(item => `./${item}`)
  };

  const indexUrl = new URL('file://statisch/versionIndex.html');
  const indexTemplateLoader = await loadFile(indexUrl);
  if (indexTemplateLoader.ok === true) {
    const indexTemplate = handlebars.compile(indexTemplateLoader.text());
    await storeFile(propsAndArgs.props[Props.IN], new URL('file://index.html'), indexTemplate(versions));
  }
};

const populateErrorList = async (propsAndArgs : PropsAndArgs) => {

  const errorTypesUrl = new URL('file://fragmenten/errorTypes.yaml');
  const errorTypesLoader = await loadFile(errorTypesUrl);

  const indexUrl = new URL('file://statisch/errorTypeIndex.html');
  const indexTemplateLoader = await loadFile(indexUrl);

  if (errorTypesLoader.ok === true && indexTemplateLoader.ok === true) {
    const errorTypes = yaml.load(errorTypesLoader.text()) as Array<ErrorType>;
    const indexTemplate = handlebars.compile(indexTemplateLoader.text());
    await storeFile(propsAndArgs.props[Props.OUT], new URL('file://foutenbeschrijvingen/index.html'), indexTemplate({ errorTypes }));
  }
};

if (process.env.NODE_ENV !== 'test') {
  const versions = getVersions();
  console.debug('Versions', JSON.stringify(versions, null, 2));
  const propsAndArgs = getPropsAndArgs();
  console.debug('Properties, flags and arguments', JSON.stringify(propsAndArgs, null, 2));
  if (propsAndArgs.args.length !== 1) {
    console.error(`Incorrect input arguments, expected either command ${Args[Args.VERSION]}, ${Args[Args.INDEX]} or ${Args[Args.CONFIG]}`);
    process.exitCode = 1;
  } else {
    console.debug("process.env.CI_COMMIT_SHORT_SHA", process.env.CI_COMMIT_SHORT_SHA);
    console.debug("process.env.CI_COMMIT_BRANCH", process.env.CI_COMMIT_BRANCH);
    console.debug("process.env.CI_COMMIT_TAG", process.env.CI_COMMIT_TAG);
    console.debug("process.env.HOME_TAG", process.env.HOME_TAG);
    console.debug("process.env.PUBLISH_TAGS", process.env.PUBLISH_TAGS);

    switch (propsAndArgs.args[0]) {
      case Args.VERSION:
        fillTemplates(propsAndArgs, versions);
        break;
      case Args.CONFIG:
        generateConfig(versions);
        break;
      case Args.INDEX:
        populateIndex(propsAndArgs).catch((reason) => {
          console.error(reason);
          process.exitCode = 1;
        });
        break;
      case Args.ERROR_TYPES:
        populateErrorList(propsAndArgs).catch((reason) => {
          console.error(reason);
          process.exitCode = 1;
        });
        break;
      default:
        console.debug('default');
        break;
    }
  }
}
